#include "shaderprogram.h"
#include "readfile.h"

ShaderProgram::ShaderProgram(string vsFilename, string fsFilename)
{
	_compiled = false;
	load(vsFilename, fsFilename);
}


ShaderProgram::~ShaderProgram()
{
}

void ShaderProgram::setMatrix(const string& name, mat4& matrix)
{
	GLint matrixId = glGetUniformLocation(_programId, name.c_str());
	glUniformMatrix4fv(matrixId, 1, GL_FALSE, &matrix[0][0]);
}

void ShaderProgram::setModelMatrix(mat4& matrix)
{
	setMatrix("model", matrix);
}
void ShaderProgram::setViewMatrix(mat4& matrix)
{
	setMatrix("view", matrix);
}
void ShaderProgram::setProjectionMatrix(mat4& matrix)
{
	setMatrix("proj", matrix);
}

void ShaderProgram::setVec3(const string& name, const vec3& v)
{
	GLint fieldId = glGetUniformLocation(_programId, name.c_str());
	glUniform3fv(fieldId, 1, &v[0]);
}

void ShaderProgram::setBool(const string& name, bool b)
{
	GLint fieldId = glGetUniformLocation(_programId, name.c_str());
	glUniform1i(fieldId, b);
}
void ShaderProgram::load(string vsFilename, string fsFilename)
{
	_compiled = false;

	if (ShaderProgram::compileShader(readFile(vsFilename), GL_VERTEX_SHADER, _vsShaderId, _errorString))
	{
		if (ShaderProgram::compileShader(readFile(fsFilename), GL_FRAGMENT_SHADER, _fsShaderId, _errorString))
		{
			_compiled = true;
		}
	}

	_programId = glCreateProgram();
	glAttachShader(_programId, _vsShaderId);
	glAttachShader(_programId, _fsShaderId);
	glLinkProgram(_programId);
}

bool ShaderProgram::compileShader(const string& code, const GLenum& shaderType, GLuint& shaderId, string& error)
{
	if (code.length() == 0)
	{
		debug() << "empty file" << endl;
		return false;
	}
	shaderId = glCreateShader(shaderType);
	const char* cCode = code.c_str();
	
	glShaderSource(shaderId, 1, &cCode, NULL);
	glCompileShader(shaderId);

	GLint compiled;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_TRUE)
	{
		return true;
	}
	else
	{
		GLint length;
		glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &length);

		char* log = new char[length];
		glGetShaderInfoLog(shaderId, length, &length, log);
		cout << string(log);
		shaderId = -1;
		return false;
	}
}

bool ShaderProgram::compiled() 
{
	return _compiled;
}

int ShaderProgram::id()
{
	return _programId;
}