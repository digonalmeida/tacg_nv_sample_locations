﻿#include "RenderModule.h"
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <sstream>

RenderModule::RenderModule()
{
	_shaderProgram = 0;
	_window = 0;
	_camera = new Camera();

	//_modelXRotationSpeed = 0.3;
	//_modelYRotationSpeed = .3;
	_modelZRotationSpeed = .9;
	rotate = false;

}


RenderModule::~RenderModule()
{

}

bool RenderModule::processMessage(const shared_ptr<Message>& message) {
	
	if (message->id() == Message::createWindow)
	{
		int width = stoi(message->get(Message::p_width, "800"));
		int height = stoi(message->get(Message::p_height, "600"));
		string title = message->get(Message::p_title, "RenderEngine");
		createWindow(width, height, title);
	}

	if (message->id() == Message::loadShader)
	{
		loadShader(
			message->get("vs"),
			message->get("fs")
			);
	}
	if (message->id() == Message::keyEvent)
	{
		KeyEvent* e = static_cast<KeyEvent*>(message.get());
		keyEvent(*e);
	}
	if (message->id() == Message::loadObject)
	{
		loadObject(
			message->get(Message::p_file),
			message->get(Message::p_texture, "")
			);
	}
	return true;
}

void RenderModule::keyEvent(const KeyEvent& e)
{
	float movementSpeed = 20.0f;

	if (e.key == GLFW_KEY_1 && e.action == GLFW_PRESS)
	{
		multisample = !multisample;
		if (multisample)
		{
			glEnable(GL_MULTISAMPLE);

		}
		else
		{
			glDisable(GL_MULTISAMPLE);
		}
		debug() << "multisample: " << (multisample ? "on" : "off") << endl;
	}
	if (e.key == GLFW_KEY_2 && e.action == GLFW_PRESS)
	{
		debug() << "sample location: center" << endl;
		_sampleLocationsManager.setAllSampleLocations(vec2(0.5, 0.5));
	}
	if (e.key == GLFW_KEY_3 && e.action == GLFW_PRESS)
	{
		debug() << "sample location: 1x1" << endl;
		_sampleLocationsManager.setAllSampleLocations(vec2(1, 1));
	}
	if (e.key == GLFW_KEY_4 && e.action == GLFW_PRESS)
	{
		debug() << "sample location: 0x0" << endl;
		_sampleLocationsManager.setAllSampleLocations(vec2(0, 0));
		
	}
	if (e.key == GLFW_KEY_5 && e.action == GLFW_PRESS)
	{
		debug() << "sample location: random" << endl;
		_sampleLocationsManager.setRandomSampleLocations();
	}

	if (e.key == GLFW_KEY_6 && e.action == GLFW_PRESS)
	{
		int count = _sampleLocationsManager.programmableSampleLocationTableSize();
		debug() << "sample locations: " << count << endl;
		debug() << endl;
		for (int i = 0; i < count; i++)
		{
			vec2 sampleLocation = _sampleLocationsManager.getSampleLocation(i);
			debug() << i << ": " << sampleLocation.x << ", " << sampleLocation.y << endl;
		}
	}

	

	if (e.key == GLFW_KEY_R && e.action == GLFW_PRESS)
	{
		rotate = !rotate;
	}

	if (e.key == GLFW_KEY_Q && e.action == GLFW_PRESS)
	{
		_camera->upSpeed = -movementSpeed;
	}
	if (e.key == GLFW_KEY_Q && e.action == GLFW_RELEASE)
	{
		_camera->upSpeed = 0;
	}
	if (e.key == GLFW_KEY_E && e.action == GLFW_PRESS)
	{
		_camera->upSpeed = movementSpeed;
	}
	if (e.key == GLFW_KEY_E && e.action == GLFW_RELEASE)
	{
		_camera->upSpeed = 0;
	}
	if (e.key == GLFW_KEY_W && e.action == GLFW_PRESS)
	{
		_camera->forwardSpeed = movementSpeed;
	}
	if (e.key == GLFW_KEY_W && e.action == GLFW_RELEASE)
	{
		_camera->forwardSpeed = 0;
	}
	if (e.key == GLFW_KEY_S && e.action == GLFW_PRESS)
	{
		_camera->forwardSpeed = -movementSpeed;
	}
	if (e.key == GLFW_KEY_S && e.action == GLFW_RELEASE)
	{
		_camera->forwardSpeed = 0;
	}
	if (e.key == GLFW_KEY_A && e.action == GLFW_PRESS)
	{
		_camera->strafeSpeed = -movementSpeed;
	}
	if (e.key == GLFW_KEY_A && e.action == GLFW_RELEASE)
	{
		_camera->strafeSpeed = 0;
	}
	if (e.key == GLFW_KEY_D && e.action == GLFW_PRESS)
	{
		_camera->strafeSpeed = movementSpeed;
	}
	if (e.key == GLFW_KEY_D && e.action == GLFW_RELEASE)
	{
		_camera->strafeSpeed = 0;
	}
	if (e.key == GLFW_KEY_ESCAPE)
	{
		_quit = true;
	}
}
void RenderModule::update(float deltaTime) 
{
	if (_window == NULL)
	{
		return;
	}

	if (rotate)
	{
		_modelMatrix = glm::rotate(_modelMatrix, _modelXRotationSpeed * deltaTime, vec3(1, 0, 0));
		_modelMatrix = glm::rotate(_modelMatrix, _modelYRotationSpeed * deltaTime, vec3(0, 1, 0));
		_modelMatrix = glm::rotate(_modelMatrix, _modelZRotationSpeed * deltaTime, vec3(0, 0, 1));
	}
	
	glfwGetCursorPos(_window, &_mouseX, &_mouseY);
	_camera->update(deltaTime, (_width / 2) - _mouseX, (_height / 2) - _mouseY);
	glfwSetCursorPos(_window, _width / 2, _height / 2);

	draw();

	// update other events like input handling 
	glfwPollEvents();
	
	// put the stuff we've been drawing onto the display
	glfwSwapBuffers(_window);

	if (glfwWindowShouldClose(_window) || _quit)
	{
		glfwTerminate();
	}
}

void RenderModule::draw()
{	
	if (_shaderProgram != 0)
	{
		glUseProgram(_shaderProgram->id());
	}
	else
	{
		debug() << "no shader loaded" << endl;
	}

	
	auto view = _camera->lookAt();
	auto proj = _camera->perspective();
	

	// wipe the drawing surface clear
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//GLint uniView = glGetUniformLocation(_shaderProgram->id(), "view");
	//glUniformMatrix4fv(uniView, 1, GL_FALSE, &view[0][0]);

	//GLint uniProj = glGetUniformLocation(_shaderProgram->id(), "proj");
	//glUniformMatrix4fv(uniProj, 1, GL_FALSE, &proj[0][0]);

	//GLint uniModel = glGetUniformLocation(_shaderProgram->id(), "model");
	//glUniformMatrix4fv(uniModel, 1, GL_FALSE, &_modelMatrix[0][0]);
	_shaderProgram->setViewMatrix(view);
	_shaderProgram->setProjectionMatrix(proj);
	_shaderProgram->setModelMatrix(_modelMatrix);


	for_each(_meshes.begin(), _meshes.end(), [this](Mesh& m) {
		m.render(*_shaderProgram);
	});
	return;
}

void  RenderModule::drawMesh(const Mesh& m)
{

}

void RenderModule::loadShader(const string& vs, const string& fs)
{
	auto shaderProgram = new ShaderProgram(vs, fs);
	if (!shaderProgram->compiled())
	{
		debug() << "shader not loaded";
		delete shaderProgram;
	}
	else
	{
		debug() << "shader loaded";
		if (_shaderProgram != 0)
		{
			delete _shaderProgram;
		}
		_shaderProgram = shaderProgram;
	}
}

void RenderModule::createWindow(int width, int height, const string& title)
{
	cout << "creating window " << endl;
	if (_window != 0) {
		glfwTerminate();
	}
	if (!glfwInit()) {
		fprintf(stderr, "ERROR: could not start GLFW3\n");
	}
	glfwWindowHint(GLFW_SAMPLES, 32);
	_window = glfwCreateWindow(width, height, title.c_str(), 0, 0);
	if (!_window) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n");
		glfwTerminate();
	}


	
	_width = width;
	_height = height;
	glfwMakeContextCurrent(_window);

	glewExperimental = GL_TRUE;
	glewInit();

	//sampleLocations
	debug() << "loading sample locations extension" << endl;
	if (_sampleLocationsManager.supported())
	{
		if (_sampleLocationsManager.enableProgrammableSampleLocations())
		{
			debug() << "programmable sample locations enabled" << endl;
		}
	}


	const char* extensions = (const char*) glGetString(GL_EXTENSIONS);
	stringstream sstream(extensions);
	int i = 0;
	while (!sstream.eof())
	{
		string s;
		sstream >> s;
		//cout << s << endl;
		i++;
	}
	cout << i << " extensions supported" << endl;
	if (glfwExtensionSupported("GL_ARB_SAMPLE_LOCATIONS"))
	{
		// The extension is supported by the current context
		debug() <<" GL_ARB_SAMPLE_LOCATIONS - suported" << endl;
	}
	else
	{
		debug() <<" GL_ARB_SAMPLE_LOCATIONS - not suported" << endl;
	}

	glEnable(GL_MULTISAMPLE);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	glfwSetInputMode(_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPos(_window, _width / 2, _height / 2);

	auto renderer = glGetString(GL_RENDERER); /* get renderer string */
	auto version = glGetString(GL_VERSION); /* version as a string */
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);

	glfwSetWindowUserPointer(_window, this);
	glfwSetKeyCallback(_window, key_callback);

	_quit = false;
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	
	RenderModule* render = static_cast<RenderModule*> ( glfwGetWindowUserPointer(window) );
	KeyEvent* event = new KeyEvent();
	event->key = key;
	event->scancode = scancode;
	event->action = action;
	event->mods = mods;
	render->_messageBus->sendMessage(shared_ptr<KeyEvent>(event));
}
void RenderModule::loadObject(const string& filename, const string& texture)
{
	
	 const aiScene* scene = scene = aiImportFile(filename.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);
	 if (scene == 0)
	 {
		 debug() << "não foi possivel carregar o objeto" << endl;
		 return;
	 }

	 vector<Mesh> meshes;
	 Mesh::loadMeshes(*scene, meshes);

	 if (texture != "")
	 {
		 for_each(meshes.begin(), meshes.end(), [this, texture](Mesh m) {
			 Material mat = m.material();
			 mat.loadTexture(texture);
			 m.setMaterial(mat);
			 _meshes.push_back(m);
		 });
	 }
	 cout << "meshes loaded: " << meshes.size() << endl;
	 for_each(meshes.begin(), meshes.end(), [this](Mesh m) {
		 _meshes.push_back(m);
	 });

	 return;
	 
}