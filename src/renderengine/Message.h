#pragma once

#include <string>
#include <map>
#include <iostream>
#include <vector>
#include <functional>
#include <memory>

using namespace std;
class Message
{
public:
	static const hash<string> Hash;
	typedef size_t MessageHash;

	Message(const MessageHash& id = Hash(""), const map<MessageHash, string>& properties = map<MessageHash, string>());
	~Message();
	static shared_ptr<Message> fromString(const string& line);
	static vector< shared_ptr<Message> > fromStream(istream& o);

	const map<MessageHash, string>& properties() const;
	MessageHash id() const;
	
	void setId(const MessageHash& id);
	void setId(const string& id);
	void setProperties(const map<MessageHash, string>& properties);
	void set(const MessageHash& propertyName, const string& value);
	void set(const string& propertyName, const string& value);
	string get(const MessageHash& propertyName, const string& defaultValue = "") const;
	string get(const string& propertyName, const string& defaultValue = "") const;

	
	static const MessageHash
		keyEvent,
		createWindow, p_width, p_height, p_title,
		loadShader,	p_vertexShader,	p_fragmentShader,
		loadObject, p_file, p_texture;



protected:
	MessageHash _id;
	map<MessageHash, string> _properties;
};

std::ostream& operator << (std::ostream& o, const Message& m);