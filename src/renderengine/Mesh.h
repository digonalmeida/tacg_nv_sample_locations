#pragma once
#include <GL/glew.h>
#include <glm\glm.hpp>
#include <vector>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Material.h"
#include "shaderprogram.h"
#include "Camera.h"
using namespace glm;
using namespace std;
class Mesh
{
public:
	~Mesh();
	Mesh();

	static vector<Mesh> loadFile(const string& filename);
	static bool loadMeshes(const aiScene& scene, vector<Mesh>& meshes, Camera* cam = 0);
	static void applyNodeHierarchy(const aiNode& node, vector<Mesh>& meshes, mat4 parentTransform);
	void loadMesh(const aiMesh& m, const vector<Material>&);
	

	float* aiVector3D2Raw(aiVector3D* vecs, size_t count, int dimensions = 3);
	template<typename T>
	void setVbo(
		GLenum bufferType,
		T* rawSource,
		size_t arraySize,
		GLuint attribIndex,
		GLuint attribSize,
		GLenum attribType,
		GLuint& vbo
		);


	GLuint vao() const;

	int elementsCount() const;

	void render(ShaderProgram&) const ;
	Material material() const;
	void setMaterial(const Material& mat);

	mat4 transform() const;
	void setTransform(const mat4&);
private:
	
	GLuint _vao;
	GLuint _verticesVbo;
	GLuint _elementsVbo;
	GLuint _normalsVbo;
	GLuint _texCoordsVbo;

	int _verticesCount;
	int _elementCount;
	int _normalsCount;
	int _texCoordsCount;
	Material _material;
	mat4 _transform;
};

template<typename T>
void Mesh::setVbo(
	GLenum bufferType,
	T* rawSource,
	size_t arraySize,
	GLuint attribIndex,
	GLuint attribSize,
	GLenum attribType,
	GLuint& vbo
	)
{
	glGenBuffers(1, &vbo);
	glBindBuffer(bufferType, vbo);
	glBufferData(
		bufferType,
		arraySize * sizeof(T),
		rawSource,
		GL_STATIC_DRAW
		);
	glVertexAttribPointer(attribIndex, attribSize, attribType, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(attribIndex);
}