#include "Material.h"
#include <string>
#include <GL/glew.h>
#include <stb_image.h>
#include <iostream>
using namespace std;


Material::Material()
{
	_diffuseColor = vec3(1.0f, 0.0f, 0.0f);
	_ambientColor = vec3(0.0f, 1.0f, 0.0f);
	_specularColor = vec3(0.0f, 0.0f, 1.0f);
	_textureVbo = -1;
}


Material::~Material()
{
}

void Material::import(const aiMaterial& mat, const string& rootDir)
{
	auto diffuseColor = aiColor3D(0.f, 0.f, 0.f);
	mat.Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor);
	auto ambientColor = aiColor3D(0.f, 0.f, 0.f);
	mat.Get(AI_MATKEY_COLOR_AMBIENT, ambientColor);
	auto specularColor = aiColor3D(0.f, 0.f, 0.f);
	mat.Get(AI_MATKEY_COLOR_SPECULAR, specularColor);

	_diffuseColor = vec3(*((vec3*)&diffuseColor));
	_ambientColor = vec3(*((vec3*)&ambientColor));
	_specularColor = vec3(*((vec3*)&specularColor));

	if (mat.GetTextureCount(aiTextureType_DIFFUSE) > 0) {
		aiString Path;

		if (mat.GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) {
			std::string FullPath = rootDir + "/" + string(Path.data);
			loadTexture(FullPath);
			cout << "texture: " << FullPath;
			//_diffuseTexture = new Texture(GL_TEXTURE_2D, FullPath.c_str());

			//if (!m_Textures[i]->Load()) {
			//	printf("Error loading texture '%s'\n", FullPath.c_str());
			//	delete m_Textures[i];
			//	m_Textures[i] = NULL;
			//	Ret = false;
			//}
		}
	}
	
}


vec3 Material::diffuseColor()
{
	return _diffuseColor;
}
vec3 Material::ambientColor()
{
	return _diffuseColor;
}
vec3 Material::specularColor()
{
	return _diffuseColor;
}

void Material::loadTexture(string filename)
{
	int x, y, n;
	int force_channels = 4;
	unsigned char* image_data = stbi_load(filename.c_str(), &x, &y, &n, force_channels);
	if (!image_data) {
		fprintf(stderr, "ERROR: could not load %s\n", filename.c_str());
	}
	int width_in_bytes = x * 4;
	unsigned char *top = NULL;
	unsigned char *bottom = NULL;
	unsigned char temp = 0;
	int half_height = y/2;

	for (int row = 0; row < half_height; row++) {
		top = image_data + row * width_in_bytes;
		bottom = image_data + (y - row - 1) * width_in_bytes;
		for (int col = 0; col < width_in_bytes; col++) {
			temp = *top;
			*top = *bottom;
			*bottom = temp;
			top++;
			bottom++;
		}
	}
	glGenTextures(1, &_textureVbo);
	glBindTexture(GL_TEXTURE_2D, _textureVbo);
	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_RGBA,
		x,
		y,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		image_data
		);
	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	GLfloat max_aniso = 0.0f;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max_aniso);
	// set the maximum!
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, max_aniso);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void Material::bind(ShaderProgram& shaderProgram) const
{
	if (_textureVbo != -1 )
	{
		glBindTexture(GL_TEXTURE_2D, _textureVbo);
		glActiveTexture(GL_TEXTURE0);
		int texLocation = glGetUniformLocation(shaderProgram.id(), "Tex");
		//glUniform1i(texLocation, _textureVbo);
		shaderProgram.setBool("material.useTexture", true);
	}
	else
	{
		shaderProgram.setBool("material.useTexture", false);
	}

	//shaderProgram.setVec3("material.ambientColor", _ambientColor);
	shaderProgram.setVec3("material.diffuseColor", _diffuseColor);
	//shaderProgram.setVec3("material.specularColor", _specularColor);


}