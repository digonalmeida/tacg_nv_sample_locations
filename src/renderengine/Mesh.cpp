#include "Mesh.h"
#include "debug.h"

Mesh::Mesh()
{
	_verticesCount = 0;
	_vao = 0;
	_verticesVbo = 0;
	_normalsVbo = 0;
	_texCoordsVbo = 0;
	_elementCount = 0;
	_transform = mat4();
}



Mesh::~Mesh()
{
}

vector<Mesh> Mesh::loadFile(const string& filename)
{

	vector<Mesh> meshes;

	return meshes;
	const aiScene* scene = scene = aiImportFile(filename.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);
	if (scene == 0)
	{
		debug() << "n�o foi possivel carregar o objeto" << endl;
	}
	else
	{
		printf("  %i animations\n", scene->mNumAnimations);
		printf("  %i cameras\n", scene->mNumCameras);
		printf("  %i lights\n", scene->mNumLights);
		printf("  %i materials\n", scene->mNumMaterials);
		printf("  %i meshes\n", scene->mNumMeshes);
		printf("  %i textures\n", scene->mNumTextures);
	}
	if (!Mesh::loadMeshes(*scene, meshes))
	{
		debug() << "n�o foi poss�vel carregar a mesh";
	}

	return meshes;
}
Material Mesh::material() const
{
	return _material;
}
void Mesh::setMaterial(const Material& mat)
{
	_material = mat;
}
bool Mesh::loadMeshes(const aiScene& scene, vector<Mesh>& meshes, Camera* camera)
{
	printf("  %i animations\n", scene.mNumAnimations);
	printf("  %i cameras\n", scene.mNumCameras);
	printf("  %i lights\n", scene.mNumLights);
	printf("  %i materials\n", scene.mNumMaterials);
	printf("  %i meshes\n", scene.mNumMeshes);
	printf("  %i textures\n", scene.mNumTextures);
	
	vector<Material> materials;
	if (camera != 0 && scene.HasCameras())
	{
		aiCamera* aicamera = scene.mCameras[0];
		camera->eye = vec3(*((vec3*) &aicamera->mPosition));
	}
	
	if (scene.HasMaterials())
	{
		for (int i = 0; i < scene.mNumMaterials; i++)
		{
			aiMaterial* material = scene.mMaterials[i];
			Material mat;
			mat.import(*material);
			materials.push_back(mat);
		}
	}
	if (scene.HasMeshes())
	{
		for (int i = 0; i < scene.mNumMeshes; i++)
		{
			aiMesh* aiMesh = scene.mMeshes[i];
			Mesh mesh;
			mesh.loadMesh(*aiMesh, materials);
			//mesh.setMaterial(mat);
			meshes.push_back(mesh);
		}
	}

	//apply node transformations
	applyNodeHierarchy(*scene.mRootNode, meshes, mat4());
	return true;
}
void Mesh::applyNodeHierarchy(const aiNode& node, vector<Mesh>& meshes, mat4 parentTransform)
{
	debug() << "applying node: " << node.mName.data << endl;
	debug() << "meshes: " << node.mNumMeshes << endl;
	debug() << "children: " << node.mNumChildren << endl;
	
	mat4 nodeTransform = transpose(*((mat4*)(&node.mTransformation)));
	
	mat4 transform = parentTransform * nodeTransform;

	for (int i = 0; i < node.mNumMeshes; i++)
	{
		meshes[node.mMeshes[i]].setTransform(transform);
	}

	for (int i = 0; i < node.mNumChildren; i++)
	{
		applyNodeHierarchy(*node.mChildren[i], meshes, transform);
	}
}
void Mesh::loadMesh(const aiMesh& aiMesh, const vector<Material>& materials)
{
	glGenVertexArrays(1, &_vao);
	glBindVertexArray(_vao);

	if (aiMesh.HasPositions())
	{
		
		GLfloat *vertices, *normals, *texCoords;
		GLuint* faces;
		_verticesCount = aiMesh.mNumVertices;
		_normalsCount = _verticesCount;
		_texCoordsCount = _verticesCount;
		_elementCount = aiMesh.mNumFaces;

		vertices = aiVector3D2Raw(aiMesh.mVertices, _verticesCount);
		setVbo<GLfloat>(GL_ARRAY_BUFFER, vertices, _verticesCount * 3, 0, 3, GL_FLOAT, _verticesVbo);

		normals = aiVector3D2Raw(aiMesh.mNormals, _normalsCount);
		setVbo<GLfloat>(GL_ARRAY_BUFFER, normals, _normalsCount * 3, 1, 3, GL_FLOAT, _normalsVbo);
		if (aiMesh.HasTextureCoords(0))
		{
			texCoords = aiVector3D2Raw(aiMesh.mTextureCoords[0], _texCoordsCount, 2);
			setVbo<GLfloat>(GL_ARRAY_BUFFER, texCoords, _texCoordsCount * 2, 2, 2, GL_FLOAT, _texCoordsVbo);
		}

		if (aiMesh.HasFaces())
		{
			faces = new GLuint[_elementCount  * 3];
			for (int i = 0; i < _elementCount; i++)
			{
				aiFace* face = &aiMesh.mFaces[i];
				if (face->mNumIndices >= 3)
				{
					for (int faceIndex = 0; faceIndex < 3; faceIndex++)
					{
						faces[(i * 3) + faceIndex] = face->mIndices[faceIndex];
					}
				}
			}

			setVbo<GLuint>(GL_ELEMENT_ARRAY_BUFFER, faces, _elementCount * 3, 4, 3, GL_UNSIGNED_INT, _elementsVbo);
		}

		delete vertices, normals, texCoords, faces;
		setMaterial(materials[aiMesh.mMaterialIndex]);
	}
}

float* Mesh::aiVector3D2Raw(aiVector3D* vecs, size_t count, int dimensions)
{
	float* rawVectorArray = new float[count * dimensions];

	for (int i = 0; i < count; i++)
	{
		rawVectorArray[(i * dimensions) + 0] = vecs[i].x;
		rawVectorArray[(i * dimensions) + 1] = vecs[i].y;
		if(dimensions == 3)
		rawVectorArray[(i * dimensions) + 2] = vecs[i].z;
	}
	return rawVectorArray;
}

void Mesh::render(ShaderProgram& shaderProgram) const {
	
	//glDisable(GL_CULL_FACE);
	glCullFace(GL_FRONT_AND_BACK);
	glEnableClientState(GL_INDEX_ARRAY);
	glPointSize(10);
	glLineWidth(3);

	_material.bind(shaderProgram);
	glBindVertexArray(_vao);

	shaderProgram.setModelMatrix(transform());
	glDrawElements(GL_TRIANGLES, 3 * _elementCount, GL_UNSIGNED_INT, (void*)0);

}

int Mesh::elementsCount() const {
	return _elementCount;
}

GLuint Mesh::vao() const {
	return _vao;
}

mat4 Mesh::transform() const {
	return _transform;
}

void Mesh::setTransform(const mat4& transform)
{
	_transform = transform;
}