#pragma once
#include <assimp/Importer.hpp>
#include <assimp/material.h>
#include <assimp/texture.h>
#include <stb_image.h>
#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include "shaderprogram.h"

using namespace std;
using namespace glm;

class Material
{
public:
	Material();
	void import(const aiMaterial& mat, const string& rootDir = "..");
	~Material();
	vec3 diffuseColor();
	vec3 ambientColor();
	vec3 specularColor();
	void loadTexture(string filename);
	void bind(ShaderProgram&) const;

private:
	vec3 _diffuseColor;
	vec3 _ambientColor;
	vec3 _specularColor;
	GLuint _textureVbo;
};

