#include "debug.h"
std::ostream& debug() {
	return std::cout;
}
std::ostream& operator<< (std::ostream& o, const string& t)
{
	o << t.c_str();
	return o;
}