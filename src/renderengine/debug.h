#pragma once
#include <iostream>
#include <algorithm>

using namespace std;


std::ostream& debug();

std::ostream& operator<< (std::ostream& o, const string& t);