#include <iostream>
#include "messagebus.h"
#include "MessageBuffer.h"
#include "debug.h"
#include <thread>
#include "Module.h"
#include "TerminalModule.h"
#include "WindowModule.h"
#include "RenderModule.h"
#include <fstream>


using namespace std;

MessageBus messageBus;
MessageBuffer buffer;
vector<Module*> modules;
vector<thread*> threads;

void processModule(Module* m);
void setupModule(Module* m);

void main()
{
	hash<string>s;
	Module* terminalMod = new TerminalModule();
	setupModule(terminalMod);
	modules.push_back(new RenderModule());
	for_each(modules.begin(), modules.end(), [](Module* module) {setupModule(module);});
	ifstream initFile("../renderengine.cfg");
	auto messages = Message::fromStream(initFile);
	for_each(messages.begin(), messages.end(), [](shared_ptr<Message> m) {
		messageBus.sendMessage(m);
	});

	thread* t = new thread(processModule, terminalMod);
	//single-thread loop
	while(true)
	{
		long long deltatime = 10;
		for_each(modules.begin(), modules.end(), [deltatime](Module* module) {
			module->processMessages(); module->update(deltatime / 1000.0f);
			this_thread::sleep_for(std::chrono::milliseconds(deltatime));
		});
	}
	t->join();
	//multi-thread loop
	//for_each(threads.begin(), threads.end(), [](thread* t) {t->join();});
}

void processModule(Module* m) {
	for (;;)
	{
		m->processMessages();
		m->update(200.0f /1000.0f);
		this_thread::sleep_for(std::chrono::milliseconds(200));
	}
}

void setupModule(Module* m) {
	messageBus.addBuffer(m->messageBuffer());
	m->setMessageBus(&messageBus);

	//thread* t = new thread(processModule, m);
	//threads.push_back(t);
}