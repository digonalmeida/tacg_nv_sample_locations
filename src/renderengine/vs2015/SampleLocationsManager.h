#pragma once

#include <glm/glm.hpp>
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <random>
using namespace glm;
using namespace std;
class SampleLocationsManager
{
public:
	SampleLocationsManager();
	~SampleLocationsManager();

	bool supported();
	bool enableProgrammableSampleLocations();
	vec2 getSampleLocation(int index);
	bool setSampleLocations(float* sampleLocations, size_t count);
	bool setSampleLocations(vector<vec2> sampleLocations);
	bool setAllSampleLocations(vec2 sl);
	bool setRandomSampleLocations();

	GLint programmableSampleLocationTableSize();
};

