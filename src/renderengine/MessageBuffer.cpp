#include "MessageBuffer.h"



MessageBuffer::MessageBuffer()
{
}


MessageBuffer::~MessageBuffer()
{
}

bool MessageBuffer::consume(shared_ptr<Message>& message) {
	_mutex.lock();
	if (_messages.size() == 0)
	{
		_mutex.unlock();
		return false;
	}

	auto first = _messages.begin();
	message = *first;
	_messages.erase(first);
	_mutex.unlock();
	return true;
}
void MessageBuffer::send(const shared_ptr<Message>& m) {
	_mutex.lock();
	_messages.push_back(m);
	_mutex.unlock();
}