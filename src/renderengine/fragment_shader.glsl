#version 400

in vec3 normal_eye;
in vec3 position_eye;
in vec2 texCoord;
out vec4 out_Color;

struct Material
{
	vec3 ambientColor;
	vec3 diffuseColor;
	vec3 specularColor;
	float specularExpoent;
	bool useTexture;
};


uniform sampler2D Tex;
uniform Material material;
uniform mat4 view;

uniform struct Camera
{
	vec3 position;
	vec3 forward;
} camera;


vec3 light_position_world  = vec3 (0.0, 10.0, 10.0);
vec3 specular_light_color = vec3 (1.0, 1.0, 1.0); // white specular colour
vec3 diffuse_light_color = vec3 (0.7, 0.7, 0.7); // dull white diffuse light colour
vec3 ambient_light_color = vec3 (.3, .3, .3); // grey ambient colour

void main()
{
	vec3 diffuseSurfaceColor;
	vec3 ambientSurfaceColor;

	float specularExpoent = 100.0;
	
	if(material.useTexture)
	{
		vec4 texColor = texture(Tex, texCoord);
		diffuseSurfaceColor = vec3(texColor.r, texColor.g, texColor.b);
		ambientSurfaceColor = diffuseSurfaceColor;
	}
	else
	{
		diffuseSurfaceColor = material.diffuseColor;
		ambientSurfaceColor = diffuseSurfaceColor;
		//diffuseSurfaceColor = vec3(texColor.r, texColor.g, texColor.b);
	}
	

	vec3 ambientColor = ambient_light_color * ambientSurfaceColor;

	//diffuse color
	vec3 light_position_eye = vec3 (vec4 (light_position_world, 1.0));
	vec3 light_direction = normalize(light_position_eye - position_eye);
	float cosTheta = clamp( dot( light_direction, normal_eye), 0,1 );
	
	vec3 diffuseColor = (cosTheta * diffuseSurfaceColor);

	//specular color
	vec3 reflection = normalize(-reflect(light_direction, normal_eye));
	vec3 eye = normalize( - vec3(view * vec4(position_eye, 1)));
	float specularIntensity = pow(max(dot(reflection,eye),0.0),specularExpoent);
	specularIntensity = clamp(specularIntensity, 0.0, 1.0); 
	
	vec3 specularColor = specular_light_color * specularIntensity;

	//out_Color = vec4 (diffuseColor, 1);
	//out_Color = vec4 (diffuseSurfaceColor, 1);
	out_Color = vec4 (ambientColor + diffuseColor + specularColor, 1);
	//out_Color = vec4(diffuse_color, 1);
	//out_Color = texColor;
	//out_Color = vec4(material.diffuseColor, 1);
	//out_Color = vec4(ambient_color,1);
}