#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/common.hpp>
using namespace glm;

class Camera
{
public:
	Camera();
	~Camera();

	vec3 eye;
	vec3 center;
	vec3 up;
	vec3 forward, right;
	float forwardSpeed, strafeSpeed, upSpeed;
	float fovy, aspect, near_plane, far_plane;

	mat4x4 lookAt();
	mat4x4 perspective();

	float horizontalAngle, verticalAngle, mouseSpeed;

	void update(float deltatime, double mouseDeltaX, double mouseDeltaY);
};

