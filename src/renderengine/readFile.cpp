#include "readfile.h"
#include "debug.h"
using namespace std;
string readFile(const string& filename)
{
	ifstream in(filename);

	if (!in.is_open())
	{
		debug() << "culdn't open " << filename << endl;
		return "";
	}
	in.seekg(0, in.end);
	std::string contents;
	in.seekg(0, std::ios::end);
	contents.resize(in.tellg());
	in.seekg(0, std::ios::beg);
	in.read(&contents[0], contents.size());
	in.close();

	return contents;
}