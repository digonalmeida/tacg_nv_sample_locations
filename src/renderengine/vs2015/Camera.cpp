#include "Camera.h"
#include <glm/detail/func_common.hpp>
#include <iostream>
using namespace std;
Camera::Camera()
{
	eye = vec3(0, 0, 0);
	up = vec3(0, 0, -1);
	forward = vec3(0, 1, 0);
	fovy = 45.0f;
	aspect = 800 / 600;
	near_plane = 0.01f;
	far_plane = 200.0f;
	mouseSpeed = 0.3f;
	verticalAngle = 0;
	horizontalAngle = 0;
	forwardSpeed = 0;
	strafeSpeed = 0;
	upSpeed = 0;
}

mat4x4 Camera::lookAt()
{
	return glm::lookAt(eye, eye + forward, up);
}
mat4x4 Camera::perspective()
{
	return glm::perspective(fovy, aspect, near_plane, far_plane);
}

Camera::~Camera()
{
}


void Camera::update(float deltatime, double mouseDeltaX, double mouseDeltaY)
{
	//cout << deltatime << ", " << mouseDeltaX << ", " << mouseDeltaY << endl;
	horizontalAngle -= mouseSpeed * deltatime * float(mouseDeltaX);
	verticalAngle += mouseSpeed * deltatime * float(mouseDeltaY);

	forward = glm::vec3(
		cos(verticalAngle) * sin(horizontalAngle),
		cos(verticalAngle) * cos(horizontalAngle),
		sin(verticalAngle)
		);

	/*
	right = glm::vec3(
		sin(horizontalAngle - 3.14f / 2.0f),
		cos(horizontalAngle - 3.14f / 2.0f),
		0
		);
		*/
	up = vec3(0, 0, 1);
	right = glm::cross(forward, up);
	
	cout << horizontalAngle << endl;
	

	eye += forward * forwardSpeed * deltatime;
	eye += right * strafeSpeed * deltatime;
	eye += up * upSpeed * deltatime;
}

