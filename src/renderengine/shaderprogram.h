#pragma once

#include <GL/glew.h>
#include <iostream>
#include <string>
#include <glm/glm.hpp>

using namespace std;
using namespace glm;

class ShaderProgram
{
public:
	ShaderProgram(string vsFilename, string fsFilename);
	~ShaderProgram();
	bool compiled();
	int id();
	void setMatrix(const string& name, mat4& matrix);
	void setModelMatrix(mat4& matrix);
	void setViewMatrix(mat4& matrix);
	void setProjectionMatrix(mat4& matrix);
	void setVec3(const string& name, const vec3&);
	void setBool(const string& name, bool b);
private:
	void load(string vsFileName, string fsFilename);
	static bool compileShader(const string& code, const GLenum& shaderType, GLuint& shaderId, string& error);
	GLuint _vsShaderId;
	GLuint _fsShaderId;
	GLuint _programId;

	bool _compiled;
	string _errorString;
};

