#include "TerminalModule.h"
#include <string>
#include <iostream>
#include "Message.h"
#include "debug.h"
#include <sstream>

using namespace std;

TerminalModule::TerminalModule()
{
}


TerminalModule::~TerminalModule()
{
}

bool TerminalModule::processMessage(const shared_ptr<Message>& message)
{
	//debug() << *message;
	return true;
}

void TerminalModule::update()
{
	string input;
	
	getline(cin, input);
	
	_messageBus->sendMessage(Message::fromString(input));
}