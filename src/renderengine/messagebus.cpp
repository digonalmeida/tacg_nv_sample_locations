#include <algorithm>
#include "messagebus.h"
#include "debug.h"

MessageBus::MessageBus() {

}
MessageBus::~MessageBus() {

}

void MessageBus::sendMessage(shared_ptr<Message> m) {
	//debug() << m;
	for_each(_buffers.begin(), _buffers.end(), [m](MessageBuffer* buffer) {
		buffer->send(m);
	});
}

void MessageBus::addBuffer(MessageBuffer* buffer) {
	_buffers.push_back(buffer);
}