#version 400

in layout(location = 0) vec3 vertex_position;
in layout(location = 1) vec3 vertex_normal;
in layout(location = 2) vec2 texture_coord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec3 normal_eye, position_eye;
out vec2 texCoord;

void main()
{
	texCoord = texture_coord;
	//normal_eye = vertex_normal;
	normal_eye = vec3( model * vec4(vertex_normal, 1.0));
	position_eye = vec3(  model * vec4(vertex_position, 0.0));
	gl_Position = proj * view * model * vec4 (vertex_position, 1.0f);
}