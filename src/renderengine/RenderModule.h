#pragma once
#include "Module.h"
#include "shaderprogram.h"
#include <graphics_common.h>
#include <vector>
#include "Mesh.h"
#include "KeyEvent.h"
#include "Camera.h"
#include "SampleLocationsManager.h"

class RenderModule :
	public Module
{
public:
	RenderModule();
	~RenderModule();

	void update(float deltatime) override;
	vector<Mesh> _meshes;

	 static friend void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

protected:
	bool processMessage(const shared_ptr<Message>& message) override;

private:
	void createWindow(int width, int height, const string& title);
	void draw();
	void keyEvent(const KeyEvent& e);
	void loadShader(const string& vs, const string& fs);
	void loadObject(const string& filename, const string& texture);
	void drawMesh(const Mesh& m);
	void rotateAll();
	ShaderProgram* _shaderProgram;
	GLFWwindow* _window;
	Camera* _camera;
	bool _quit;
	double _mouseX, _mouseY;
	int _width, _height;
	glm::mat4x4 _modelMatrix;

	float _modelXRotationSpeed,
		_modelYRotationSpeed,
		_modelZRotationSpeed;
	bool rotate;

	bool multisample;
	bool antialiasing;
	SampleLocationsManager _sampleLocationsManager;
};

