#pragma once

#include "MessageBuffer.h"
#include "messagebus.h"
#include "debug.h"

class Module
{
	
public:
	Module();
	~Module();
	void processMessages();
	
	virtual void update(float deltatime) {};
	void setMessageBus(MessageBus* messageBus);
	MessageBuffer* messageBuffer();

private:
	MessageBuffer _messageBuffer;

protected:
	virtual bool processMessage(const shared_ptr<Message>& m) = 0;
	MessageBus* _messageBus;
};
