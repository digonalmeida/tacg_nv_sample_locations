#include "Module.h"



Module::Module()
{

}


Module::~Module()
{
}

void Module::processMessages()
{
	shared_ptr<Message> m;
	while (_messageBuffer.consume(m))
	{
		processMessage(m);
	}
}

void Module::setMessageBus(MessageBus* messageBus)
{
	_messageBus = messageBus;
}

MessageBuffer* Module::messageBuffer()
{
	return &_messageBuffer;
}