#pragma once
#include "Message.h"
#include <graphics_common.h>

class KeyEvent :
	public Message
{
public:
	KeyEvent();
	~KeyEvent();
	int key, scancode, action, mods;
};

