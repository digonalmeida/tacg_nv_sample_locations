#pragma once

#include <vector>
#include <mutex>

#include "Message.h"

using namespace std;

class MessageBuffer
{
public:
	MessageBuffer();
	~MessageBuffer();
	bool consume(shared_ptr<Message>& message);
	void send(const shared_ptr<Message>& m);

private:
	vector<shared_ptr<Message>> _messages;
	mutex _mutex;
};

