#pragma once

#include <vector>
#include "Message.h"
#include "MessageBuffer.h"

using namespace std;

class MessageBus{
public:
	MessageBus();
	~MessageBus();
	void sendMessage(shared_ptr<Message> msg);
	void addBuffer(MessageBuffer* buffer);

private:
	vector<MessageBuffer*> _buffers;

};