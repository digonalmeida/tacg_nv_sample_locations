#include "Message.h"
#include <sstream>
#include <algorithm>

#define SET_MESSAGE(MSG,STR) \
 const Message::MessageHash Message::MSG = Message::Hash(STR)

const std::hash<std::string> Message::Hash;

SET_MESSAGE(keyEvent, "keyEvent");
SET_MESSAGE(createWindow , "createWindow");
SET_MESSAGE(loadShader, "loadShader");
SET_MESSAGE(p_vertexShader, "vs");
SET_MESSAGE(p_fragmentShader, "fs");
SET_MESSAGE(p_width, "w");
SET_MESSAGE(p_height, "h");
SET_MESSAGE(p_title, "title");
SET_MESSAGE(loadObject, "loadObject");
SET_MESSAGE(p_file, "file");
SET_MESSAGE(p_texture, "texture");

Message::Message(const MessageHash& id, const map<MessageHash, string>& properties)
{
	_id = id;
	_properties = properties;
}

Message::~Message()
{

}

shared_ptr<Message> Message::fromString(const string& line)
{
	stringstream sstream(line);

	shared_ptr<Message> m(new Message());
	bool first = true;
	string s;
	string propertyName = "";
	int propertyId = 0;
	while (!sstream.eof())
	{
		sstream >> s;
		if (first)
		{
			m->setId(s);
			first = false;
		}
		else
		{
			if (s[0] == '-')
			{
				propertyName = s.substr(1);
				m->set(propertyName, "");
			}
			else
			{
				if (propertyName == "")
				{
					propertyName = to_string(propertyId++);
				}
				m->set(propertyName, s);
				propertyName = "";
			}
		}
	}
	return m;
}

vector<shared_ptr<Message> > Message::fromStream(istream& i)
{
	vector<shared_ptr<Message> > messages;
	string line;
	while (!i.eof())
	{
		getline(i, line);
		messages.push_back(Message::fromString(line));
	}
	return messages;
}
Message::MessageHash Message::id() const {
	return _id;
}

string Message::get(const Message::MessageHash& propertyName, const string& defaultValue) const
{
	auto it = _properties.find(propertyName);
	if (it == _properties.end())
	{
		return defaultValue;
	}
	return it->second;
}
string Message::get(const string& propertyName, const string& defaultValue) const
{
	return get(Hash(propertyName), defaultValue);
}

const map<Message::MessageHash, string>& Message::properties() const {
	return _properties;
}
void Message::set(const MessageHash& propertyName, const string& value) {
	_properties[propertyName] = value;
}
void Message::set(const string& propertyName, const string& value)
{
	set(Hash(propertyName), value);
}
void Message::setId(const MessageHash& id) {
	_id = id;
}

void Message::setId(const string& id) {
	_id = Hash(id);
}
void Message::setProperties(const map<MessageHash, string>& properties) {
	_properties = properties;
}

std::ostream& operator << (std::ostream& o, const Message& m){
	o << "[MSG] " << m.id() << endl;
	for_each(m.properties().begin(), m.properties().end(), [&o](auto pair) {
		o << "    " << pair.first << ": " << pair.second << endl;
	});
	return o;
}