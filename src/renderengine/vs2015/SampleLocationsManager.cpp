#include "SampleLocationsManager.h"
#include "debug.h"


using namespace glm;

SampleLocationsManager::SampleLocationsManager()
{
}


SampleLocationsManager::~SampleLocationsManager()
{
}


bool SampleLocationsManager::supported()
{
	if (glfwExtensionSupported("GL_NV_sample_locations"))
	{
		return true;
	}
	return false;
}

bool SampleLocationsManager::enableProgrammableSampleLocations()
{
	glFramebufferParameteri(GL_DRAW_FRAMEBUFFER, GL_FRAMEBUFFER_PROGRAMMABLE_SAMPLE_LOCATIONS_NV, 1);
	GLenum errorNo = glGetError();
	if (errorNo != GL_NO_ERROR)
	{
		debug() << gluErrorString(errorNo) << endl;
		return false;
	}
	return true;
}

vec2 SampleLocationsManager::getSampleLocation(int index)
{
	vec2 sampleLocation;
	glGetMultisamplefv(GL_PROGRAMMABLE_SAMPLE_LOCATION_NV, index, &sampleLocation[0]);
	return sampleLocation;
}

GLint SampleLocationsManager::programmableSampleLocationTableSize()
{
	GLint tableSize;
	glGetIntegerv(GL_PROGRAMMABLE_SAMPLE_LOCATION_TABLE_SIZE_NV, &tableSize);
	return tableSize;
}

bool SampleLocationsManager::setSampleLocations(float* sampleLocations, size_t count)
{
	if (count == 0)
	{
		debug() << "0 sample locations set" << endl;
		return false;
	}
	glFramebufferSampleLocationsfvNV(GL_FRAMEBUFFER, 0, count, sampleLocations);
	GLenum errorNo = glGetError();
	if (errorNo != GL_NO_ERROR)
	{
		debug() << gluErrorString(errorNo) << endl;
		return false;
	}
	return true;
}
bool SampleLocationsManager::setSampleLocations(vector<vec2> sampleLocations)
{
	float* rawSampleLocations = new float[sampleLocations.size() * 2];
	for (int i = 0; i < sampleLocations.size(); i++)
	{
		rawSampleLocations[(2 * i) + 0] = sampleLocations[i].x;
		rawSampleLocations[(2 * i) + 1] = sampleLocations[i + 1].y;
	}
	bool ok = setSampleLocations(rawSampleLocations, sampleLocations.size());
	delete[] rawSampleLocations;
	return ok;
}

bool SampleLocationsManager::setAllSampleLocations(vec2 sl)
{
	vector<vec2> sampleLocations;
	int size = programmableSampleLocationTableSize();
	for (int i = 0; i < size; i++)
	{
		sampleLocations.push_back(sl);
	}
	return setSampleLocations(sampleLocations);

}
bool SampleLocationsManager::setRandomSampleLocations()
{
	vector<vec2> sampleLocations;
	int size = programmableSampleLocationTableSize();
	for (int i = 0; i < size; i++)
	{
		vec2 sl;
		sl.x = rand();
		sl.y = rand();
		sampleLocations.push_back(sl);
	}
	return setSampleLocations(sampleLocations);
}