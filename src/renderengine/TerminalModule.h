#pragma once
#include "Module.h"

class TerminalModule : public Module
{
public:
	TerminalModule();
	~TerminalModule();
	void update();
	bool processMessage(const shared_ptr<Message>& m);
};

